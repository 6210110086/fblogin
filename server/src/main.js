const bodyParser = require('body-parser')
const express = require('express')
const cors = require('cors')
const axios = require('axios')
const jwt = require('jsonwebtoken')
const app = express()
const port = 8080

const TOKEN_SECRET = '42b1e0daf156370e8654fa3ffa22442572175d42e2cd55a39f519832c6dfe1760f3ee25dd73677529932eafc28c14e5cf8b3bb6701afc2b8c42e4c70858296d5'

const authenticated = (req, res, next) => {
        const auth_header = req.headers['authorization']
        const token = auth_header && auth_header.split(' ')[1]
        if(!token)
            return res.sendStatus(401)
        jwt.verify(token,TOKEN_SECRET, (err, info) => {
            if(err) return res.sendStatus(403)
            req.username = info.username
            next()
        })
}
app.use(cors())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/api/info', authenticated, (req,res) => {
    res.send({ok: 1, username: req.username})
})

app.post('/api/login', bodyParser.json(), async(req,res)  => {
    let token = req.body.token
    let result = await axios.get('https://graph.facebook.com/me',{
     params:{
            fields: 'id,name,email',
            access_token: token
        }
    })
    console.log(result.data)
    if(!result.data.id){
        res.send(403)
        return
    }
        let data = {
            username: result.data.email
        }
       let access_token = jwt.sign(data, TOKEN_SECRET, {expiresIn: '1800s'})
       res.send({access_token, username: data.username})
       
    })

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})